package ru.tsc.felofyanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnerDTORepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(@Nullable String userId, @NotNull String projectId);
}
