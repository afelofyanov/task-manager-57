package ru.tsc.felofyanov.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class TaskDTORepository extends AbstractUserOwnerDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager, TaskDTO.class);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("FROM " + getModelName() + " WHERE user_id = :userId AND project_Id = :projectId", clazz)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        entityManager.createQuery("DELETE FROM " + getModelName() + " WHERE user_id = :userId AND project_Id = :projectId")
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .executeUpdate();
    }
}
