package ru.tsc.felofyanov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.model.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.model.IProjectService;
import ru.tsc.felofyanov.tm.model.Project;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }
}
