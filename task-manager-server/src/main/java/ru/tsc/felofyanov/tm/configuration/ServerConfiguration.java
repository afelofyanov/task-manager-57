package ru.tsc.felofyanov.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.tsc.felofyanov.tm.api.service.IDatabaseProperty;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.tsc.felofyanov.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull IDatabaseProperty databaseProperty) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(Environment.USER, databaseProperty.getDatabaseUserName());
        settings.put(Environment.PASS, databaseProperty.getDatabaseUserPassword());
        settings.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecondL2Cache());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseRegionFactoryClass());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseProviderConfigurationFileResourcePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);

        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }
}
