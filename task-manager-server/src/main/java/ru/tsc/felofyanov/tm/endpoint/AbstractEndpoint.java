package ru.tsc.felofyanov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.felofyanov.tm.api.service.IAuthService;
import ru.tsc.felofyanov.tm.dto.model.SessionDTO;
import ru.tsc.felofyanov.tm.dto.request.AbstractUserRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.system.AccessDeniedException;

@Getter
@Controller
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Nullable
    @Autowired
    private IAuthService authService;

    protected SessionDTO check(@Nullable AbstractUserRequest request, @Nullable Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();

        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable SessionDTO result = getAuthService().validateToken(token);
        @Nullable Role userRole = result.getRole();
        if (userRole != role) throw new AccessDeniedException();
        return result;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getAuthService().validateToken(token);
    }
}
