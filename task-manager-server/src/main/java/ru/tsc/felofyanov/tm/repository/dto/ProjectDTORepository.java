package ru.tsc.felofyanov.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

@Getter
@Repository
@Scope("prototype")
public class ProjectDTORepository extends AbstractUserOwnerDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager, ProjectDTO.class);
    }
}
