package ru.tsc.felofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.service.model.IProjectService;
import ru.tsc.felofyanov.tm.api.service.model.IProjectTaskService;
import ru.tsc.felofyanov.tm.api.service.model.ITaskService;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;

public class ProjectTaskServiceTest extends AbstractTest {

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @Before
    @Override
    public void init() {
        super.init();
        projectService = CONTEXT.getBean(IProjectService.class);
        taskService = CONTEXT.getBean(ITaskService.class);
        projectTaskService = CONTEXT.getBean(IProjectTaskService.class);
    }

    @After
    @Override
    public void close() {
        super.close();
    }

    @Test
    public void bindTaskToProject() {
        @Nullable Project project = projectService.createByUserId(testUser.getId(), "DEMO");
        @Nullable Task task = taskService.createByUserId(testUser.getId(), "MEGA");

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(null, "test", "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject("test", null, "test"));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject("test", "test", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject("test", "test", "test"));

        Assert.assertNull(task.getProject());
        projectTaskService.bindTaskToProject(testUser.getId(), project.getId(), task.getId());
        @Nullable Task taskBind = taskService.findOneById(task.getId());

        Assert.assertNotNull(taskBind);
        Assert.assertNotNull(taskBind.getProject());
    }

    @Test
    public void unbindTaskFromProject() {
        @Nullable Project project = projectService.createByUserId(testUser.getId(), "DEMO");
        @Nullable Task task = taskService.createByUserId(testUser.getId(), "MEGA");
        projectTaskService.bindTaskToProject(testUser.getId(), project.getId(), task.getId());

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(null, "test", "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject("test", null, "test"));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject("test", "test", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject("test", "test", "test"));

        @Nullable Task taskBeforeUnbind = taskService.findOneById(task.getId());
        Assert.assertNotNull(taskBeforeUnbind.getProject());

        projectTaskService.unbindTaskFromProject(testUser.getId(), project.getId(), task.getId());
        @Nullable Task taskAfterUnbind = taskService.findOneById(task.getId());

        Assert.assertNotNull(taskAfterUnbind);
        Assert.assertNull(taskAfterUnbind.getProject());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(null, "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById("test", null));

        @Nullable Project project = projectService.createByUserId(testUser.getId(), "DEMO");
        @Nullable Task task = taskService.createByUserId(testUser.getId(), "MEGA");
        projectTaskService.bindTaskToProject(testUser.getId(), project.getId(), task.getId());

        Assert.assertNotNull(taskService.findOneById(task.getId()));
        Assert.assertNotNull(projectService.findOneById(project.getId()));

        projectTaskService.removeProjectById(testUser.getId(), project.getId());

        Assert.assertNull(taskService.findOneById(task.getId()));
        Assert.assertNull(projectService.findOneById(project.getId()));
    }
}
