package ru.tsc.felofyanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by index.";
    }

    @Override
    @EventListener(condition = "@projectUpdateByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final ProjectUpdateByIndexRequest request =
                new ProjectUpdateByIndexRequest(getToken(), index, name, description);
        getProjectEndpoint().updateProjectByIndex(request);
    }
}
