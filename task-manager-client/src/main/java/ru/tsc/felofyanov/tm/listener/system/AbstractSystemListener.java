package ru.tsc.felofyanov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;
import ru.tsc.felofyanov.tm.listener.AbstractListener;
import ru.tsc.felofyanov.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
