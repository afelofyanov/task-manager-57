package ru.tsc.felofyanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ServerVersionRequest;
import ru.tsc.felofyanov.tm.dto.response.ServerVersionResponse;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Component
public final class VersionListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show applicant version.";
    }

    @Override
    @EventListener(condition = "@versionListener.getName() == #event.name || @versionListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("Client: " + getPropertyService().getApplicationVersion());
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(new ServerVersionRequest());
        System.out.println("Client: " + response.getVersion());
    }
}