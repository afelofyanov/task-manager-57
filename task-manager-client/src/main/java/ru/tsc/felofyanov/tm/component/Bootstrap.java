package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.service.ILoggerService;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.listener.AbstractListener;
import ru.tsc.felofyanov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.felofyanov.tm.util.SystemUtil;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@Getter
@Component
public final class Bootstrap {

    private static final String PACKAGE_COMMANDS = "ru.tsc.felofyanov.tm.command";

    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    @Autowired
    private List<AbstractListener> commands;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    private void prepareStartup() {

        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SHUTTING DOWN**");
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();

        while (true) processCommand();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND");
            @NotNull final String command = TerminalUtil.nextLine();
            boolean cmdCheck = commands.stream().anyMatch(cmd -> Objects.equals(cmd.getName(), command));
            boolean argCheck = commands.stream().anyMatch(cmd -> Objects.equals(cmd.getArgument(), command));
            if (!cmdCheck && !argCheck) throw new CommandNotSupportedException();
            publisher.publishEvent(new ConsoleEvent(command));
            System.err.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        publisher.publishEvent(new ConsoleEvent(arg));
        return true;
    }
}
