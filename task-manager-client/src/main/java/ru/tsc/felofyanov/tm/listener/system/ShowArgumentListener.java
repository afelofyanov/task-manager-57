package ru.tsc.felofyanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ShowArgumentListener extends AbstractSystemListener {

    @Nullable
    @Autowired
    private AbstractListener[] commands;

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show argument list.";
    }

    @Override
    @EventListener(condition = "@showArgumentListener.getName() == #event.name " +
            "|| @showArgumentListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        for (AbstractListener command : commands)
        {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
}
