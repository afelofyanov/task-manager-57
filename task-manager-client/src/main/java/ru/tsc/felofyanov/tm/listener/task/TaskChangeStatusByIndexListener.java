package ru.tsc.felofyanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

    @Override
    @EventListener(condition = "@taskChangeStatusByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");

        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);

        @NotNull final TaskChangeStatusByIndexRequest request =
                new TaskChangeStatusByIndexRequest(getToken(), index, status);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }
}
