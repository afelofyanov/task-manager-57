package ru.tsc.felofyanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ShowListener extends AbstractSystemListener {

    @Nullable
    @Autowired
    private AbstractListener[] commands;

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list.";
    }

    @Override
    @EventListener(condition = "@showListener.getName() == #event.name || @showListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        for (AbstractListener command : commands) {
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }
}
